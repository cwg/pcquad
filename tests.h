/* integration/tests.h
 *
 * Copyright (C) 1996, 1997, 1998, 1999, 2000, 2007 Brian Gough
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

void cqf1(double *vals, unsigned n, void *params);
void cqf2(double *vals, unsigned n, void *params);
void cqf3(double *vals, unsigned n, void *params);
void cqf4(double *vals, unsigned n, void *params);
void cqf5(double *vals, unsigned n, void *params);
void cqf6(double *vals, unsigned n, void *params);
void cqf7(double *vals, unsigned n, void *params);
void cqf8(double *vals, unsigned n, void *params);
void cqf9(double *vals, unsigned n, void *params);
void cqf10(double *vals, unsigned n, void *params);
void cqf11(double *vals, unsigned n, void *params);
void cqf12(double *vals, unsigned n, void *params);
void cqf13(double *vals, unsigned n, void *params);
void cqf14(double *vals, unsigned n, void *params);
void cqf15(double *vals, unsigned n, void *params);
void cqf16(double *vals, unsigned n, void *params);
void cqf17(double *vals, unsigned n, void *params);
void cqf18(double *vals, unsigned n, void *params);
void cqf19(double *vals, unsigned n, void *params);
void cqf20(double *vals, unsigned n, void *params);
void cqf21(double *vals, unsigned n, void *params);
void cqf22(double *vals, unsigned n, void *params);
void cqf23(double *vals, unsigned n, void *params);
void cqf24(double *vals, unsigned n, void *params);
void cqf25(double *vals, unsigned n, void *params);
