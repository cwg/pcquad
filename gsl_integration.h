/* integration/gsl_integration.h
 *
 * Copyright (C) 1996, 1997, 1998, 1999, 2000, 2007 Brian Gough
 * Copyright (C) 2016 Christoph Groth
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef __GSL_INTEGRATION_H__
#define __GSL_INTEGRATION_H__
#include <stdlib.h>

enum {
    GSL_SUCCESS = 0,
    GSL_EDOM = 1,               /* input domain error, e.g sqrt(-1) */
    GSL_EINVAL = 4,             /* invalid argument supplied by user */
    GSL_ENOMEM = 8,             /* malloc failed */
    GSL_EBADTOL = 13,           /* user specified an invalid tolerance */
    GSL_EDIVERGE = 22,          /* integral or series is divergent */
};

struct gsl_function_struct {
    void (*function) (double *vals, unsigned n, void *params);
    void *params;
};

typedef struct gsl_function_struct gsl_function;

#define GSL_FN_EVAL(F, vals, n) (*((F)->function))(vals, n, (F)->params)


/* Cquad integration - Pedro Gonnet */

/* Data of a single interval */
typedef struct {
    double a, b;
    double c[64];
    double fx[33];
    double igral, err;
    int depth, rdepth, ndiv;
} gsl_integration_cquad_ival;


/* The workspace is just a collection of intervals */
typedef struct {
    size_t size;
    gsl_integration_cquad_ival *ivals;
    size_t *heap;
} gsl_integration_cquad_workspace;

gsl_integration_cquad_workspace
    *gsl_integration_cquad_workspace_alloc(const size_t n);

void gsl_integration_cquad_workspace_free(gsl_integration_cquad_workspace * w);

int gsl_integration_cquad(const gsl_function *f, double a, double b,
                          double epsabs, double epsrel,
                          gsl_integration_cquad_workspace * ws,
                          double *result, double *abserr, size_t *nevals);

#endif /* __GSL_INTEGRATION_H__ */
