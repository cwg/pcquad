CFLAGS = -lm -g

SRC = $(wildcard *.c)
OBJ = $(SRC:.c=.o)

.PHONY: run_test
run_test: test
	./test

test: $(OBJ)
	gcc $(CFLAGS) $(OBJ) -o test

%.o:    %.c
	gcc $(CFLAGS) -c -Wall -Wextra -o $@ $<

.PHONY: clean
clean:
	rm -f $(OBJ)
	rm -f test

.DELETE_ON_ERROR:
