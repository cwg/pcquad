/* integration/test.c
 *
 * Copyright (C) 1996, 1997, 1998, 1999, 2000, 2007 Brian Gough, Gerard Jungman
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <float.h>

#include "gsl_integration.h"
#include "tests.h"

static unsigned int tests = 0;
static unsigned int passed = 0;
static unsigned int failed = 0;

static unsigned int verbose = 0;


static void update(int s)
{
    tests++;

    if (s == 0) {
        passed++;
    } else {
        failed++;
    }
}

void gsl_test(int status, const char *test_description, ...)
{
    update(status);

    if (status || verbose) {
        printf(status ? "FAIL: " : "PASS: ");
        {
            va_list ap;
            va_start(ap, test_description);
            vprintf(test_description, ap);
            va_end(ap);
        }

        if (status && !verbose)
            printf(" [%u]", tests);

        printf("\n");
        fflush(stdout);
    }
}

void
gsl_test_rel(double result, double expected, double relative_error,
             const char *test_description, ...)
{
    int status;

    /* Check for NaN vs inf vs number */

    if (isnan(result) || isnan(expected)) {
        status = isnan(result) != isnan(expected);
    } else if (isinf(result) || isinf(expected)) {
        status = isinf(result) != isinf(expected);
    } else if ((expected > 0 && expected < DBL_MIN)
               || (expected < 0 && expected > -(DBL_MIN))) {
        status = -1;
    } else if (expected != 0) {
        status =
            (fabs(result - expected) / fabs(expected) > relative_error);
    } else {
        status = (fabs(result) > relative_error);
    }

    update(status);

    if (status || verbose) {
        printf(status ? "FAIL: " : "PASS: "); {
            va_list ap;
            va_start(ap, test_description);
            vprintf(test_description, ap);
            va_end(ap);
        }

        if (status == 0) {
            if (strlen(test_description) < 45) {
                printf(" (%g observed vs %g expected)", result, expected);
            } else {
                printf(" (%g obs vs %g exp)", result, expected);
            }
        } else {
            printf(" (%.18g observed vs %.18g expected)", result,
                   expected);
        }

        if (status == -1) {
            printf(" [test uses subnormal value]");
        }

        if (status && !verbose)
            printf(" [%u]", tests);

        printf("\n");
        fflush(stdout);
    }
}

void
gsl_test_int(int result, int expected, const char *test_description, ...)
{
    int status = (result != expected);

    update(status);

    if (status || verbose) {
        printf(status ? "FAIL: " : "PASS: "); {
            va_list ap;

            va_start(ap, test_description);
            vprintf(test_description, ap);
            va_end(ap);
        }
        if (status == 0) {
            printf(" (%d observed vs %d expected)", result, expected);
        } else {
            printf(" (%d observed vs %d expected)", result, expected);
        }

        if (status && !verbose)
            printf(" [%u]", tests);

        printf("\n");
        fflush(stdout);
    }
}

int gsl_test_summary(void)
{
    if (verbose && 0)           /* FIXME: turned it off, this annoys me */
        printf("%d tests, passed %d, failed %d.\n", tests, passed, failed);

    if (failed != 0) {
        return EXIT_FAILURE;
    }

    if (tests != passed + failed) {
        if (verbose)
            printf("TEST RESULTS DO NOT ADD UP %d != %d + %d\n",
                   tests, passed, failed);
        return EXIT_FAILURE;
    }

    if (passed == tests) {
        if (!verbose)           /* display a summary of passed tests */
            printf("Completed [%d/%d]\n", passed, tests);

        return EXIT_SUCCESS;
    }

    return EXIT_FAILURE;
}

gsl_function make_function(void (*f) (double *, unsigned, void *), double *p)
{
    gsl_function f_new;

    f_new.function = f;
    f_new.params = p;

    return f_new;
}

void my_error_handler(const char *reason, const char *file,
                      int line, int err);


int main(void)
{
    typedef void (*fptr) (double *, unsigned, void *);

    static const fptr funs[25] = {
        &cqf1, &cqf2, &cqf3, &cqf4, &cqf5, &cqf6, &cqf7,
        &cqf8, &cqf9, &cqf10, &cqf11, &cqf12, &cqf13, &cqf14, &cqf15,
        &cqf16, &cqf17,
        &cqf18, &cqf19, &cqf20, &cqf21, &cqf22, &cqf23, &cqf24, &cqf25
    };

    static const double ranges[50] = {
        0, 1, 0, 1, 0, 1, -1, 1, -1, 1, 0, 1, 0, 1, 0, 1, 0, 1,
        0, 1, 0, 1, 0, 1, 0, 1, 0, 10, 0, 10, 0, 10, 0, 1, 0, M_PI,
        0, 1, -1, 1, 0, 1, 0, 1, 0, 1, 0, 3, 0, 5
    };
    static const double f_exact[25] = {
        1.7182818284590452354, 0.7, 2.0 / 3, 0.4794282266888016674,
        1.5822329637296729331, 0.4, 2, 0.86697298733991103757,
        1.1547005383792515290, 0.69314718055994530942,
        0.3798854930417224753,
        0.77750463411224827640, 0.49898680869304550249,
        0.5, 1, 0.13263071079267703209e+08, 0.49898680869304550249,
        0.83867634269442961454, -1, 1.5643964440690497731,
        0.16349494301863722618, -0.63466518254339257343,
        0.013492485649467772692, 17.664383539246514971, 7.5
    };

    double result, abserr;
    size_t neval;
    int fid;

    /* Loop over the functions... */
    for (fid = 0; fid < 25; fid++) {
        gsl_integration_cquad_workspace *ws =
            gsl_integration_cquad_workspace_alloc(200);
        gsl_function f = make_function(funs[fid], NULL);
        double exact = f_exact[fid];

        /* Call our quadrature routine. */
        int status = gsl_integration_cquad(&f, ranges[2 * fid],
                                           ranges[2 * fid + 1], 0.0,
                                           1.0e-12,
                                           ws, &result, &abserr,
                                           &neval);

        gsl_test_rel(result, exact, 1e-12, "cquad f%d", fid);
        gsl_test(fabs(result - exact) > 5.0 * abserr,
                 "cquad f%d error (%g actual vs %g estimated)", fid,
                 fabs(result - exact), abserr);
        gsl_test_int(status, GSL_SUCCESS, "cquad return code");

        gsl_integration_cquad_workspace_free(ws);
    }

    exit(gsl_test_summary());
}
