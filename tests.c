/* integration/tests.c
 *
 * Copyright (C) 1996, 1997, 1998, 1999, 2000, 2007 Brian Gough
 * Copyright (C) 1996, 1997, 1998, 1999, 2000 Gerard Jungman
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <math.h>

#include "tests.h"

#define UNUSED(x) (void)(x)

/* The test functions. */
void cqf1(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        vals[i] = exp(x);
    }
}

void cqf2(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        vals[i] = x >= 0.3;
    }
}

void cqf3(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        vals[i] = sqrt(x);
    }
}

void cqf4(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        vals[i] = (23.0 / 25) * cosh(x) - cos(x);
    }
}

void cqf5(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        double x2 = x * x;
        vals[i] = 1.0 / (x2 * (x2 + 1) + 0.9);
    }
}

void cqf6(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        vals[i] = x * sqrt(x);
    }
}

void cqf7(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        vals[i] = 1.0 / sqrt(x);
    }
}

void cqf8(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        double x2 = x * x;
        vals[i] = 1.0 / (1 + x2 * x2);
    }
}

void cqf9(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        vals[i] = 2.0 / (2 + sin(10 * M_PI * x));
    }
}

void cqf10(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        vals[i] = 1.0 / (1 + x);
    }
}

void cqf11(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        vals[i] = 1.0 / (1 + exp(x));
    }
}

void cqf12(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        vals[i] = x / (exp(x) - 1.0);
    }
}

void cqf13(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        vals[i] = sin(100 * M_PI * x) / (M_PI * x);
    }
}

void cqf14(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        vals[i] = sqrt(50.0) * exp(-50 * M_PI * x * x);
    }
}

void cqf15(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        vals[i] = 25.0 * exp(-25 * x);
    }
}

void cqf16(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        vals[i] = 50 / M_PI * (2500 * x * x + 1);
    }
}

void cqf17(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        double t1 = 50 * M_PI * x, t2;
        t2 = sin(t1) / t1;
        vals[i] = 50 * t2 * t2;
    }
}

void cqf18(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        vals[i] = cos(cos(x) + 3 * sin(x) + 2 * cos(2 * x) +
                      3 * sin(2 * x) + 3 * cos(3 * x));
    }
}

void cqf19(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        vals[i] = log(x);
    }
}

void cqf20(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        vals[i] = 1 / (x * x + 1.005);
    }
}

void cqf21(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        vals[i] = 1 / cosh(10 * (x - 0.2) * 2) +
            1 / cosh(100 * (x - 0.4) * 4) + 1 / cosh(1000 * (x - 0.6) * 8);
    }
}

void cqf22(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        vals[i] = 4 * M_PI * M_PI * x * sin(20 * M_PI * x) * cos(2 * M_PI * x);
    }
}

void cqf23(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        double t = 230 * x - 30;
        vals[i] = 1 / (1 + t * t);
    }
}

void cqf24(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        vals[i] = floor(exp(x));
    }
}

void cqf25(double *vals, unsigned n, void *params)
{
    UNUSED(params);
    unsigned i;
    for (i = 0; i < n; ++i) {
        double x = vals[i];
        vals[i] = (x < 1) * (x + 1) + (1 <= x && x <= 3) * (3 - x) + (x > 3) * 2;
    }
}
